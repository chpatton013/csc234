#include <stdio.h>
#include <math.h>

double getDouble(char letter);
double quadratic(char oper, double a, double b, double c);
double determ(double a, double b, double c);

int main(void)
{
   int i;
   double a, b, c;

   for (i=0; i<3; i++)
   {   
      a=getDouble('a');
      b=getDouble('b');
      c=getDouble('c');
      
      if (determ(a, b, c) > 0) {
         printf("The x-values for this equation are %.3f and %.3f\n", quadratic('+',a,b,c), quadratic('-',a,b,c));
      }
      
      if (determ(a, b, c) < 0) {
         printf("This equation has complex roots\n");
      }
   }
   
   return 0;
}

double getDouble(char letter)
{
   double x;
   
   printf("Enter value %c: ", letter);
   scanf("%lf", &x);
   
   return x;
}

double determ(double a, double b, double c)
{
   double d;
   
   d = b*b-4*a*c;
   
   return d;
}

double quadratic(char oper, double a, double b, double c)
{
   double x;
   
   if (oper == '+') x = (-b+sqrt(determ(a,b,c)))/(2*a);
   if (oper == '-') x = (-b-sqrt(determ(a,b,c)))/(2*a);
   
   return x;
}
