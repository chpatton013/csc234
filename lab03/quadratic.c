#include <stdio.h>
#include <math.h>

double getDouble(char letter);
double quadratic(double a, double b, double c);

int main(void)
{
   int i;
   double a, b, c;

   for (i=0; i<3; i++)
   {   
      a=getDouble('a');
      b=getDouble('b');
      c=getDouble('c');
      
      printf("One x-value for this equation is %.3f\n", quadratic(a, b, c));
   }
   
   return 0;
}

double getDouble(char letter)
{
   double x;
   
   printf("Enter value %c: ", letter);
   scanf("%lf", &x);
   
   return x;
}

double quadratic(double a, double b, double c)
{
   double x;
   
   x = (sqrt(b*b-4*a*c)-b)/(2*a);
   
   return x;
}
