#include <stdio.h>
#include <math.h>

double distCalc(double x1, double y1, double x2, double y2);

int main(void) {
   double x1, y1, x2, y2;
   
   printf("Enter x1: ");
   scanf("%lf", &x1);
   
   printf("Enter y1: ");
   scanf("%lf", &y1);
   
   printf("Enter x2: ");
   scanf("%lf", &x2);
   
   printf("Enter y2: ");
   scanf("%lf", &y2);
   
   printf("The distance between (%.2f, %.2f) and (%.2f, %.2f) is %.2f\n", x1, y1, x2, y2, distCalc(x1,y1,x2,y2));
   
   return 0;
}

double distCalc(double x1, double y1, double x2, double y2) {
   double d;
   double deltaX = x2-x1;
   double deltaY = y2-y1;

   d = sqrt(pow(deltaX,2)+pow(deltaY,2));

   return d;
}
