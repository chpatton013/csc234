/* Project 1
 *
 * Name: Christopher Patton
 * Instructor: P.Hatalsky
 * Section: 02
 */


/* This program simulates the result of a scenario involving
   projectile motion, conservation of momentum, and blunt
   forced trauma. On a frictionless ice rink, an object is
   thrown at a 45 degree angle at a target which is 'd'
   meters away.
   
   Input:    "How much do you weigh (pounds)? "wSkt
             "How far away is your professor (meters)? "dist
             "Will you throw a...? "obj
             // (if obj == o)
             "Enter the weight of your object in KG: "wObj
   
   Output:   1st:
             "Nice throw!  "
             
             2nd:
             wObj <= 0.1:
                "You're going to get an F!"
             wObj (0.1, 1.0]:
                "Make sure your professor is OK."
             wObj > 1.0 && dist < 20:
                "How far away is the hospital?"
             wObj > 1.0 && dist >= 20:
                "RIP professor."
             
             3rd:
             "Velocity of skater: "vSkt" m/s"
             
             4th:
             vSkt < 0.2:
                "\nMy grandmother skates faster than you!"
             vSkt >= 1.0:
                "\nLook out for that railing!!!"
   
   Functs:   main (call input getFuntions; output results)
             getWeight (in = obj; out = wObj)
             getVelObject (in = d; out = vObj)
             getVelSkater (in = wSkt, wObj, vObj; out = vSkt)
   
   Vars:     char obj;
             int wSkt, dist;
             double wObj, vObj, vSkt;
             
   
   Formulas: vObj = (G*d/2)^(1/2)
             vSkt = wObj*vObj/(wSkt*LBS2KG)

*/


#include <stdio.h>
#include <math.h>

#define G 9.8          /* accel due to gravity            */
#define LBS2KG 0.4536  /* pounds to kgs conversion factor */
#define TKG 0.1        /* weight of tomato in kg          */
#define PKG 1.0        /* weight of pie in kg             */
#define RKG 3.0        /* weight of rock in kg            */

double getWeight(char obj);
double getVelObject(int d);
double getVelSkater(int wSkt, double wObj, double vObj);

int main() {
   char obj;
   int wSkt, dist;
   double wObj, vObj, vSkt;
   
   /* input */
   printf("How much do you weigh (pounds)? ");
   scanf("%d", &wSkt);

   printf("How far away is your professor (meters)? ");
   scanf("%d", &dist);
   
   printf("Will you throw a rotten (t)omato, banana cream (p)ie, (r)ock, or (o)ther? ");
   scanf(" %c", &obj);
   
   /* calculations */
   wObj = getWeight(obj);
   vObj = getVelObject(dist);
   vSkt = getVelSkater(wSkt, wObj, vObj);
   
   /* output */
   printf("\nNice throw!  ");
   
   
   if (wObj <= 0.1)
      printf("You're going to get an F!");
   
   if (wObj > 0.1 && wObj <= 1.0)
      printf("Make sure your professor is OK.");
   
   if (wObj > 1.0 && dist < 20)
         printf("How far away is the hospital?");
      
   if (wObj > 1.0 && dist >= 20)
         printf("RIP professor.");
   
   
   printf("\nVelocity of skater: %.3f m/s", vSkt);
   
   
   if (vSkt < 0.2)
      printf("\nMy grandmother skates faster than you!");
   
   
   if (vSkt >= 1.0)
      printf("\nLook out for that railing!!!");
   
   printf("\n");
   
   return 0;
}

double getWeight(char obj) {
   double x;
   
   if (obj == 't') x = TKG;
   if (obj == 'p') x = PKG;
   if (obj == 'r') x = RKG;
   if (obj == 'o') {
      printf("Enter the weight of the object in KG: ");
      scanf("%lf", &x);
   }
   
   return x;
}

double getVelObject(int dist) {
   double x = sqrt(G*dist/2);
   return x;
}

double getVelSkater(int wSkt, double wObj, double vObj) {
   double x = wObj*vObj/(wSkt*LBS2KG);
   return x;
}



/* fin */
