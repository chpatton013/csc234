#include <stdio.h>
#include <string.h>

#define MAX_LEN 30

int myStrlen(char str[]);
void wordReverse(char str[], char rStr[]);
void pigLatin(char str[], char pigStr[]);
int isVowel(char letter);
int sentinel(char *str, char *quitStr);

int main(void){
   char str[MAX_LEN], rStr[MAX_LEN], rPigStr[MAX_LEN], quitStr[] = {'q', 'u', 'i', 't'};
   int loop = 1;
   
   while(loop != 0){
      printf("Enter a word: ");
      scanf("%s", str);
      
      printf("Length of your word: %d\n", myStrlen(str));
      
      wordReverse(str, rStr);
      printf("Reversed: %s\n", rStr);
      
      pigLatin(rStr, rPigStr);
      printf("Reversed Pig Latin: %s\n", rPigStr);
      
      loop = sentinel(str, quitStr);
      
      if(loop != 0) printf("\n");
   }
   
   return 0;
}

int myStrlen(char str[]){
   int i;

   for(i = 0; str[i] != '\0'; i++);

   return i;
}

void wordReverse(char str[], char rStr[]){
   int i, size = myStrlen(str);
   
   for(i = 0; i < size; i++)
      rStr[i] = str[size-1-i];

   rStr[size] = '\0';
}

void pigLatin(char str[], char pigStr[]){
   int i, j, size = myStrlen(str);
   int v;
   
   if(isVowel(str[0]) == 1){
      for(i = 0; i < size; i++)
         pigStr[i] = str[i];
      
      pigStr[size] = 'w';
      pigStr[size + 1] = 'a';
      pigStr[size + 2] = 'y';
      pigStr[size + 3] = '\0';
   }
   
   else {
      for(v = 0; isVowel(str[v]) == 0; v++);
      
      for(i = 0; str[v + i] != '\0'; i++)
         pigStr[i] = str[v + i];
      
      for(j = 0; j != v; j++)
         pigStr[i + j] = str[j];
      
      pigStr[size] = 'a';
      pigStr[size + 1] = 'y';
      pigStr[size + 2] = '\0';
   }
}

int isVowel(char letter){
   if(letter=='a' || letter=='e' || letter=='i' || letter=='o' || letter=='u')
      return 1;
   
   else if(letter=='A' || letter=='E' || letter=='I' || letter=='O' || letter=='U')
      return 1;
   
   else return 0;
}

int sentinel(char *str, char *quitStr){
   int i;
   
   if(myStrlen(str) != 4) return 1;
   
   for(i = 0; i < 4; i++)
      if(str[i] != quitStr[i]) return 1;
   
   return 0;
}
