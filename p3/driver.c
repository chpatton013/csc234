/* Driver file for program 4.
 * This driver should be tested with the driver_input input file
 */

#include <stdio.h>
#include "sudokusolver.h"

int main(void) {
        int puzzle[9][9];  /* array to store the puzzle */

        readSudoku(puzzle);  /* read in the puzzle */

        printf ("%d\n", checkValid(puzzle, 0, 2, 5));  /* place 5 in row 0, column 2 = invalid */
        printf ("%d\n", checkValid(puzzle, 0, 2, 8));  /* place 8 in row 0, column 2 = invalid */
        printf ("%d\n", checkValid(puzzle, 0, 2, 9));  /* place 9 in row 0, column 2 = invalid */

        printf ("%d\n", checkValid(puzzle, 1, 1, 8));  /* place 8 in row 1, column 1 = valid */
        printf ("%d\n", checkValid(puzzle, 0, 8, 7));  /* place 7 in row 0, column 8 = valid */
        printf ("%d\n", checkValid(puzzle, 8, 0, 6));  /* place 6 in row 8, column 0 = valid */

        printf ("%d\n", checkValid(puzzle, 0, 0, 6));  /* place 6 in row 0, column 0 = square not empty */

        return 0;
}

void readSudoku(int puzzle[][9]) {
   FILE *fp = fopen("driver_input", "r");
   int i, j, status;
	
   for(i=0; i<9; i++)
	   for(j=0; j<9; j++)
		   status = fscanf(fp, "%1d", &puzzle[i][j]);
	
	fclose(fp);
}

int checkValid(int puzzle[][9], int row, int col, int num) {
   int i, /* row */
	    j; /* col */
	
	/* check if empty */
	if(puzzle[row][col] != 0)
	   return (-1);
	
	/* check row */
	for(i=0; i<9; i++)
	   if(num == puzzle[row][i] && i != col)
		   return 0;
	
	/* check col */
	for(j=0; j<9; j++)
	   if(num == puzzle[j][col] && j != row)
		   return 0;
			
	/* check local square */
   for(i=(row/3)*3; i<(row/3+1)*3; i++)
	   for(j=(col/3)*3; j<(col/3+1)*3; j++)
		   if(num == puzzle[i][j] && i != row && j != col)
			   return 0;
	
	return 1;
}