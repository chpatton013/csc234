#include <stdio.h>
#include "sudokusolver.h"

int backtrack(int puzzle[][9], int solverData[2], int row, int col, int num);
int countZeroes(int puzzle[][9]);

int main(void) {
    int puzzle[9][9], solverData[2]={ 0 }, i, j, k, check=1, zeroes=0, single=0;

    readSudoku(puzzle);

    printf("Input puzzle:\n\n");  /* print input */
    for (i=0; i<9; i++) {
        for (j=0; j<9; j++)
            printf("%1d", puzzle[i][j]);
        printf("\n");
    }
    printf("\n");

    zeroes=countZeroes(puzzle);

    for (i=0; i<9; i++)  /* check correct input */
        for (j=0; j<9; j++)
            if (puzzle[i][j]!=0 && checkValid(puzzle, i, j, puzzle[i][j])==0)
                check=0;

    for (i=0; i<zeroes; i++)  /* run single candidates */
        for (j=0; j<9; j++)
            for (k=0; k<9; k++)
                if (singleCandidate(puzzle, j, k)==1)
                    single++;

    bruteForce(puzzle, solverData);  /* run brute force */

    if (check==0)
        printf("Puzzle is not solvable.\n");
    else {
        if (solverData[1]>0)
            printf("Solved by brute force with backtracking.\n");
        else if (solverData[0]>0)
            printf("Solved by brute force with no backtracking.\n");
        else
            printf("Solved by single candidate technique.\n");
    }

    printf("Single candidates found: %d\n", single);
    printf("Failed attempts: %d\n", solverData[0]);
    printf("Backtracks: %d\n\n", solverData[1]);


    if (check==1) {
        printf("Solution:\n\n");  /* print solution */
        printSudoku(puzzle);
    }

    return 0;
}

void readSudoku(int puzzle[][9]) {
    FILE *fp = fopen("t10.in", "r");
    int i, j, status;

    for (i=0; i<9; i++) {
        for (j=0; j<9; j++)
            status = fscanf(fp, "%1d", &puzzle[i][j]);
        printf("Enter line %d: ", i+1);
    }

    printf("\n\n");

    fclose(fp);
}

int checkValid(int puzzle[][9], int row, int col, int num) {
    int i, j;

    /* check row */
    for (i=0; i<9; i++)
        if (num == puzzle[row][i] && i != col)
            return 0;

    /* check col */
    for (j=0; j<9; j++)
        if (num == puzzle[j][col] && j != row)
            return 0;

    /* check local square */
    for (i=(row/3)*3; i<(row/3+1)*3; i++)
        for (j=(col/3)*3; j<(col/3+1)*3; j++)
            if (num == puzzle[i][j] && i != row && j != col)
                return 0;

    /* check if empty */
    if (puzzle[row][col] != 0)
        return (-1);

    return 1;
}

void printSudoku(int puzzle[][9]) {
    int i, j;

    for (i=0; i<9; i++) {

        for (j=0; j<9; j++) {
            printf("%d", puzzle[i][j]);
            if (j==2 || j==5)
                printf(" | ");
        }

        printf("\n");
        if (i==2 || i==5)
            printf("---------------\n");
    }

    printf("\n");
}

int singleCandidate(int puzzle[][9], int row, int col) {
    int i, single, count=0;

    for (i=1; i<10; i++) {
        if (checkValid(puzzle, row, col, i) == 1) {
            count++;
            single=i;
        }
    }

    if (count == 1)
        puzzle[row][col] = single;
    else return 0;

    return 1;
}

void bruteForce(int puzzle[][9], int solverData[2]) {
    backtrack(puzzle, solverData, 0, 0, 1);
}

int backtrack(int puzzle[][9], int solverData[2], int row, int col, int num) {
    int err=1;

    if (puzzle[row][col] != 0) {
        if (col<8) err=backtrack(puzzle, solverData, row, col+1, 1);
        else if (row<8) err=backtrack(puzzle, solverData, row+1, 0, 1);
        return err;
    }

    else {
        if (checkValid(puzzle, row, col, num)==1) {
            puzzle[row][col]=num;
            if (col<8) err=backtrack(puzzle, solverData, row, col+1, 1);
            else if (row<8) err=backtrack(puzzle, solverData, row+1, 0, 1);
            if (err==0) {
                puzzle[row][col]=0;
                if (num<9) return backtrack(puzzle, solverData, row, col, num+1);
                else {
                    solverData[1]++;
                    return 0;
                }
            }
        }
        else {
            solverData[0]++;
            if (num<9) return backtrack(puzzle, solverData, row, col, num+1);
            else {
                solverData[1]++;
                return 0;
            }
        }
    }
    return 1;
}

int isSolved(int puzzle[][9]) {
    int i, j;

    for (i=0; i<9; i++)
        for (j=0; j<9; j++)
            if (puzzle[i][j] == 0)
                return 0;

    return 1;
}

int countZeroes(int puzzle[][9]) {
    int i, j, k=0;

    for (i=0; i<9; i++)
        for (j=0; j<9; j++)
            if (puzzle[i][j]==0)
                k++;

    return k;
}
