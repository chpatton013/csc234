#include <stdio.h>

int getNumber();
void displayMessage(int occurrences);

int main(void) {
   FILE *inp;
   int   num,
         current = 0,
         qnum = 0,
         qitems = 0,
         first = 0,
         last = 0,
         status = 0;
   inp = fopen("numbers.txt", "r");
   
   num = getNumber();

   status = fscanf(inp, "%d", &current);
   
   while (status != EOF) {
      qitems++;
      if (current == num) {
         if (first == 0) first = qitems;
         last = qitems;
         qnum++;
      }
      status = fscanf(inp, "%d", &current);
   }
   
   printf("\nThis list contains %d numbers.\n", qitems);
   printf("First occurrence of '%d': %d\n", num, first);
   printf("Last occurrence of '%d': %d\n", num, last);
   
   displayMessage(qnum);
   fclose(inp);
   
   return 0;
}

int getNumber() {
   int num;
   
   printf("Enter a number to find: ");
   scanf("%d", &num);
   
   while (num < 0) {
      printf("Enter a number to find: ");
      scanf("%d", &num);
   }   
   return num;
}

void displayMessage(int occurrences) {
   if (occurrences == 0)
      printf("Uh-oh, try a different input file!");
   else if (occurrences <= 5)
      printf("Not too many in this list.");
   else if (occurrences <= 10)
      printf("Quite a few were found!");
   else printf("Wow, that's a lot!");
   printf("\n");
}
