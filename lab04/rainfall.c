#include <stdio.h>

#define SENTINEL -1

int main(void)
{
   int rainfall = 0;
   int sum = 0;
   int count = 0;
   double avg = 0;

   printf("Enter rainfall; negative to quit.\n");
   while (rainfall > SENTINEL)
   {
      count++;
      sum = sum + rainfall;
      printf("%d: ", count);
      scanf("%d", &rainfall);
   }

   count--;   
   avg = (double)sum/count;

   printf("Sum of %d rainfall measurements is %d\n", count, sum);

   if (count > 0)
      printf("Average rainfall is %.2f\n", avg);
   else
      printf("Average rainfall is 0\n");

   return 0;
}
