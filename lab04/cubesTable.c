/* CPE 101 Lab 4
   Christopher Patton
*/
#include <stdio.h>
#include <math.h>

int get_table_size();
int get_first();
int get_increment();
void show_table(int size, int first, int inc);

int main(void) {
   int table_size, first, inc;
	  
   /* loop until sentinel is reached */
   table_size = get_table_size();
   while (table_size != 0)
   {
      first = get_first();
      inc = get_increment();
      show_table(table_size, first, inc);
      table_size = get_table_size();
   }
   return 0;
}

int get_table_size() {
   int size;
   printf("Enter number of rows in table (0 to end): ");
   scanf("%d", &size);
   while (size < 0) {
      printf("Size must be non-negative.\n");
      printf("Enter number of rows in table (0 to end): ");
      scanf("%d", &size);
   }
   return size;
}

int get_first() {
   int first;
   printf("Enter the value of the first number in the table: ");
   scanf("%d", &first);
   while (first < 0) {
      printf("First number must be non-negative.\n");
      printf("Enter the value of the first number in the table: ");
      scanf("%d", &first);
   }
   return first;
}

int get_increment() {
   int inc;
   printf("Enter the increment between rows: ");
   scanf("%d", &inc);
   while (inc < 0) {
      printf("Increment must be non-negative.\n");
      printf("Enter increment between rows: ");
      scanf("%d", &inc);
   }
   return inc;
}

void show_table(int size, int first, int inc) {
   int count, num=first, cube, sum=0;
   
   printf("A cube table of size %d will appear here starting with %d.\n", size, first);   
   printf("Number  Cube\n");
   
   for(count = 0; count < size; count++) {
      printf("%-8d", num);
      cube = pow(num,3);
      printf("%d\n", cube);
      
      num = num + inc;
      sum = sum + cube;
   }
   printf("\nThe sum of cubes is %d\n\n", sum);
}
