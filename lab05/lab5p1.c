#include <stdio.h>
#include <math.h>

#define SENTINEL -1

int main(void) {
   int scores[35] = {0};
   int num = 0;
   int i;
   
   while (scores[num-1] != SENTINEL  &&  num < 35) {
      num++;
      
      printf("Enter test score  %d: ", num);
      scanf("%d", &scores[num-1]);
   }
   
   if (scores[num-1] == -1) num--;
   
   printf("\nNumber of scored entered: %d\n", num);
   printf("Test scores entered     :");
   
   for (i = 0; i < num; i++) {
      printf(" %d", scores[i]);
   }
   
   printf("\n");
      
   return 0;
}
