#include <stdio.h>
#include <math.h>

#define SENTINEL -1

double calcAvg(int array[], int size);

int main(void) {
   int scores[35] = {0};
   int num = 0;
   int i;
   
   while (scores[num-1] != SENTINEL  &&  num < 35) {
      num++;
      
      printf("Enter test score  %d: ", num);
      scanf("%d", &scores[num-1]);
   }

   if (scores[num-1] == -1) num--;
      
   printf("\nNumber of scores entered: %d\n", num);
   printf("Test scores entered     :");
   
   for (i = 0; i < num; i++) {
      printf(" %d", scores[i]);
   }
   
   printf("\nAverage test scores     : %.1f\n", calcAvg(scores, num));
      
   return 0;
}

double calcAvg(int array[], int size) {
   int sum = 0;
   int i;
   
   for (i = 0; i < size; i++) {
      sum = sum + array[i];
   }
   
   return ((double)sum/size);
}
