#include <stdio.h>
#include <math.h>

#define SENTINEL -1

double calcAvg(int array[], int size);
int findValue(int array[], int size, int value);

int main(void) {
   int scores[35] = {0};
   int num = 0;
   int i;
   int value;
   int find;
   
   while (scores[num-1] != SENTINEL  &&  num < 35) {
      num++;
      
      printf("Enter test score  %d: ", num);
      scanf("%d", &scores[num-1]);
   }
   
   printf("Enter test score to search for: ");
   scanf("%d", &value);
   
   if (scores[num-1] == -1) num--;
   find = findValue(scores, num, value);
      
   printf("\nNumber of scores entered: %d\n", num);
   printf("Test scores entered     :");
   
   for (i = 0; i < num; i++) {
      printf(" %d", scores[i]);
   }
   
   printf("\nAverage test scores     : %.1f", calcAvg(scores, num));
   
   if (find != SENTINEL) printf("\nThe score %d first occurs at index %d.", value, find);
   else printf("\nThe score %d does not occur in the array.", value);
   
   printf("\n");
   
   return 0;
}

double calcAvg(int array[], int size) {
   int sum = 0;
   int i;
   
   for (i = 0; i < size; i++) {
      sum = sum + array[i];
   }
   
   return ((double)sum/size);
}

int findValue(int array[], int size, int value) {
   int i;
   
   for (i = 0; i < size; i++) {
      if (array[i] == value) return i;
   }
   
   return -1;
}
