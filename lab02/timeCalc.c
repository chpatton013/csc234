/*
     timeCalc.c
     Christopher Patton
     Converts time in seconds to days, hours, and minutes
*/

#include <stdio.h>

int main(void)
{
   int input = 0;
   int milliseconds = 0;
   int seconds = 0;
   int days = 0;
   int hours = 0;
   int minutes = 0;

   /* Obtain input */
   printf("Enter number of milliseconds to convert: ");
   scanf("%d", &input);
   
   /* Assign input to milliseconds */
   milliseconds = input;
   
   /* Seconds in milleconds */
   seconds = milliseconds / 1000;
   milliseconds = milliseconds % 1000;
   
   /* Minutes in seconds */
   minutes = seconds / 60;
   seconds = seconds % 60;

   /* Hours in miuntes */
   hours = minutes / 60;
   minutes = minutes % 60;
   
   /* Days in hours */
   days = hours / 24;
   hours = hours % 24;
   
   /* Display output */
   printf("In %d milliseconds, There are:\n%d days, %d hours, %d minutes, and %d seconds\n",
   input, days, hours, minutes, seconds);

   return 0;
}
