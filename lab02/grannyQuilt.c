/* 
 * grannyQuilt.c
 * Christopher patton
 * quilt cost calculator
 */

#include <stdio.h>
#include <math.h>

#define PI 3.141593
#define BUTTON 2
#define FRINGE 20

int main(void)
{

   /* Input Variables */
   int diam = 0;
   int cost = 0;
   int numb = 0;
   
   /* Output Variables */
   double area = 0;
   double circ = 0;
   int tcost = 0;
   int pcost = 0;
   
   /* Input Prompts */
   printf("Enter integer diameter of circle in feet: ");
   scanf("%d", &diam);
   
   printf("Enter fabric cost per square foot in cents: ");
   scanf("%d", &cost);
   
   printf("Enter number of people sewing: ");
   scanf("%d", &numb);
   printf("\n");

   /* Calculations */
   area = PI*(diam/2)*(diam/2);
   circ = PI*diam;
   tcost = ceil(diam*2*12*BUTTON+circ*FRINGE+area*cost);
   pcost = ceil(tcost/numb);
   
   /* Output Results */
   printf("The circle area is %.2f square feet.\n", area);
   printf("The circumference is %.2f feet.\n", circ);
   printf("Total cost is %d cents.\n", tcost);
   printf("Cost per person is %d cents.\n", pcost);
   
   return 0;
}
