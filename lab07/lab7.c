#include <stdio.h>
#include <string.h>

#define SECONDS_PER_HOUR 3600
#define SECONDS_PER_MIN 60

typedef struct {
   char name[20];
   double distance;
   int prSec;
   int count;
} Run;

typedef struct {
   int hours;
   int minutes;
   int seconds;
} Time;

int readRunsFromFile(Run r[], char fileName[]);
Time secsToHMS(int totalSeconds);
int findRun(Run r[], int numRuns, char name[]);
void writeFile(Run r[], int numRuns);
void displayRuns(Run r[], int numRuns);
int addRun(Run r[], int numRuns);

int main(void) {
   Run r[20];
   int numRuns = readRunsFromFile(r, "run.data");
   char loop;
   
   displayRuns(r, numRuns);
	
   printf("(a)dd run or (q)uit: ");
   scanf("%c", &loop);

   if(loop == 'q') printf("\n");

   while(loop != 'q') {
      printf("\n");
		
      if (loop == 'a') numRuns = addRun(r, numRuns);

      printf("\n");
      displayRuns(r, numRuns);
	   
      printf("(a)dd run or (q)uit: ");
      scanf(" %c", &loop);
      printf("\n");
	
   }
	
   writeFile(r, numRuns);
   
   printf("Happy trails!\n");

   return 0;
}

int readRunsFromFile(Run r[], char fileName[]) {
   FILE *fp = NULL;
   int i = 0, status;
   
	fp = fopen(fileName, "r");
	
	status = fscanf(fp, "%s %lf %d %d", r[i].name, &r[i].distance, &r[i].prSec, &r[i].count);
	
   while (status != EOF) {
      i++;
      status = fscanf(fp, "%s %lf %d %d", r[i].name, &r[i].distance, &r[i].prSec, &r[i].count);
   }
	
   fclose(fp);
   
   return i;
}

Time secsToHMS(int totalSeconds) {
   Time time;

   time.seconds = totalSeconds;

   time.hours = time.seconds/SECONDS_PER_HOUR;
   time.seconds = time.seconds%SECONDS_PER_HOUR;
   time.minutes = time.seconds/SECONDS_PER_MIN;
   time.seconds = time.seconds%SECONDS_PER_MIN;
   
   return time;
}

int findRun(Run runs[], int numRuns, char name[]) {
   int i, index;

   index = -1;

   for (i=0; i<numRuns && index==-1; i++)
      if (!strcmp(name, runs[i].name))
         index = i;
         
   return index;
}

void writeFile(Run r[], int numRuns) {
   FILE *fp = NULL;
   int i;

   fp = fopen("run.data", "w");

   for (i = 0; i<numRuns; i++) {
      fprintf(fp, "%s %.2f %d %d\n", r[i].name, r[i].distance, r[i].prSec, r[i].count);
   }
   fclose(fp);
}

void displayRuns(Run r[], int numRuns) {
   int i;
   Time pace, pr;
	
	printf("Run Report:\n\n");
   printf("Name                 Distance   PR       Pace    Count\n");
   printf("------------------------------------------------------\n");
   
   for(i=0; i<numRuns; i++) {
      pr = secsToHMS(r[i].prSec);
      pace = secsToHMS(r[i].prSec/r[i].distance);
      
      printf("%-20s", r[i].name);
      printf("%9.2f  ", r[i].distance);
      printf("%02d:%02d:%02d  ", pr.hours, pr.minutes, pr.seconds);
      printf("%02d:%02d  ", pace.minutes, pace.seconds);
      printf("%5d\n", r[i].count);
   }
}

int addRun(Run r[], int numRuns) {
   int i, j;
   char name[20];
   Time pr;
         
   printf("Run Name: ");
   scanf("%s", name);
   
   i = findRun(r, numRuns, name);
   
   if (i == -1) {
      for(j=0; j<strlen(name); j++) r[numRuns].name[j]=name[j];
      r[numRuns].name[j] = '\0';
      
      printf("Distance in Miles: ");
      scanf("%lf", &r[numRuns].distance);
      
      printf("Time (hh mm ss): ");
      scanf("%d %d %d", &pr.hours, &pr.minutes, &pr.seconds);
      
      r[numRuns].prSec = (pr.hours*3600 + pr.minutes*60 + pr.seconds);
      r[numRuns].count = 1;
      numRuns++;
   }
   
   else {
      printf("Time (hh mm ss): ");
      scanf("%d %d %d", &pr.hours, &pr.minutes, &pr.seconds);
      
      if ((pr.hours*3600 + pr.minutes*60 + pr.seconds) < r[i].prSec)
         r[i].prSec = (pr.hours*3600 + pr.minutes*60 + pr.seconds);
      
      r[i].count++;
   }
   
   return numRuns;
}
