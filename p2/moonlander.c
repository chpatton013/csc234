/* Project 2
 *
 * Name: Christopher Patton
 * Instructor: Paul Hatalsky
 */

#include <math.h>
#include <stdio.h>

#define GRAVITY 1.62

void showWelcome(void);
int getFuel(void);
double getAltitude(void);
void displayLMState(int time, double altitude, double velocity, int fuel, int fuelRate);
int getFuelRate(int fuel);
double updateAcceleration(double gravity, int fuelRate);
double updateAltitude(double altitude, double velocity, double acceleration);
double updateVelocity(double velocity, double acceleration);
int updateFuel(int fuel, int rate);
void displayLMLandingStatus(double velocity);

int main()
{
   int fuelRate = 0;        /* fuel rate in liters per second, 0-9 valid */
   int elapsedTime = 0;     /* time LM has been piloted in seconds */
   int fuel;                /* in liters - to be initialized by player */
   double acceleration = 0; /* in meters per second per second */
   double velocity = 0;     /* in meters per second */
   double altitude;         /* in meters - to be initialized by player */

   showWelcome();
   altitude = getAltitude();
   fuel = getFuel();
   printf("\nLM state at retrorocket cutoff\n");

   while (altitude > 0) {
      displayLMState(elapsedTime, altitude, velocity, fuel, fuelRate);

      fuelRate = getFuelRate(fuel);      
      fuel = updateFuel(fuel, fuelRate);
      acceleration = updateAcceleration(GRAVITY, fuelRate);
      altitude = updateAltitude(altitude, velocity, acceleration);
      velocity = updateVelocity(velocity, acceleration);

      elapsedTime++;

      if (fuel == 0) {
         
         while (altitude > 0) {
            printf("OUT OF FUEL - Elapsed Time: %3d Altitude: %7.2f Velocity: %8.2f\n", elapsedTime, altitude, velocity);

            elapsedTime++;

            acceleration = updateAcceleration(GRAVITY, 0);
            altitude = updateAltitude(altitude, velocity, acceleration);
            velocity = updateVelocity(velocity, acceleration);
         }

         printf("\n");

      }
   }
   
   printf("LM state at landing/impact\n");
   displayLMState(elapsedTime, altitude, velocity, fuel, fuelRate);
   displayLMLandingStatus(velocity);
   
   return 0;
}


void showWelcome(void) {
   printf("\nWelcome aboard the Lunar Module Flight Simulator\n\n");
   printf("   To begin you must specify the LM's initial altitude\n");
   printf("   and fuel level.  To simulate the actual LM use\n");
   printf("   values of 1300 meters and 500 liters, respectively.\n\n");
   printf("   Good luck and may the force be with you!\n\n");
}


int getFuel(void) {
   int x;

   printf("Enter the initial amount of fuel on board the LM (in liters): ");
   scanf(" %d", &x);
   
   while (x < 1) {
      printf("ERROR: Amount of fuel must be positive, please try again\n");
      printf("Enter the initial amount of fuel on board the LM (in liters): ");
      scanf(" %d", &x);
   }

   return x;
}


double getAltitude(void) {
   double x;

   printf("Enter the initial altitude of the LM (in meters): ");
   scanf(" %lf", &x);
   
   while (x < 1 || x > 9999) {
      printf("ERROR: Altitude must be between 1 and 9999, inclusive, please try again\n");
      printf("Enter the initial altitude of the LM (in meters): ");
      scanf(" %lf", &x);
   }

   return x;
}


void displayLMState(int time, double altitude, double velocity, int fuel, int fuelRate) {
   printf("Elapsed Time: %4d s\n", time);
   printf("        Fuel: %4d l\n", fuel);
   printf("        Rate: %4d l/s\n", fuelRate);
   printf("    Altitude: %7.2f m\n", altitude);
   printf("    Velocity: %7.2f m/s\n\n", velocity);
}


int getFuelRate(int fuel) {
   int x;

   printf("Enter fuel rate (0-9, 0=freefall, 5=constant velocity, 9=max thrust): ");
   scanf(" %d", &x);
   
   while (x < 0 || x > 9) {
      printf("ERROR: Fuel rate must be between 0 and 9, inclusive\n\n");
      printf("Enter fuel rate (0-9, 0=freefall, 5=constant velocity, 9=max thrust): ");
      scanf(" %d", &x);
   }
   
   if (fuel < x) x = fuel;
   
   return x;
}


double updateAcceleration(double gravity, int fuelRate) {
   double x = gravity * ((fuelRate / 5.0) - 1);
   return x;
}


double updateAltitude(double altitude, double velocity, double acceleration) {
   double x = altitude + velocity + (acceleration / 2.0);
   if (x < 0) return 0;
   else return x;
}


double updateVelocity(double velocity, double acceleration) {
   double x = velocity + acceleration;
   return x;
}


int updateFuel(int fuel, int rate) {
   int x = fuel - rate;
   return x;
}


void displayLMLandingStatus(double velocity) {
   printf("Status at landing - ");
   
   if (velocity <= -10) printf("Ouch - that hurt!\n");
   else if (velocity < -1) printf("Enjoy your oxygen while it lasts!\n");
   else printf("The eagle has landed!\n");
}
